# Broedkast server

De docker-container in deze git-repository ontvangt meetgegevens van een broedmachine en geeft deze (met behulp van grafana) weer aan de gebruiker.

## Graphite

Graphite takes in raw text from any computer and puts it into a graph.

### API (gegevens versturen)

```bash
export SERVER=graphite-server.example.com
export PORT=2003
echo "temperature 37.5 `date +%s`" | nc ${SERVER} ${PORT} -q0
```

and

```bash
export SERVER=graphite-server.example.com
export PORT=2003
echo "humidity 90 `date +%s`" | nc ${SERVER} ${PORT} -q0
```

### Meetgegevens bekijken via graphite

Bezoek `graphite-server.example.com` om te controleren of meetgegevens zijn ontvangen.

## Meetgegevens kijken via grafana

Grafana is een webservice waarmee meetgegevens kunnen worden omgezet in grafieken.

### Graphite aan grafana linken

- Voeg een nieuwe `data source` toe van type `graphite`.
- In het `host` veld, typ `graphite`.
- Klik op `Save & Test`

### Meetgegevens visualiseren

- Maak een nieuw `dashboard` aan, met als gegevensbron `graphite`.
- Voeg een grafiek toe voor `temperature` en `humidity`.

# Gegevens vanuit de broedmachine versturen

## Netcat toevoegen aan de broedmachine-software:

```bash
cd buildroot && make menuconfig
```

```
Target packages -> [*] Show packages that are also provided by busybox
Target packages -> Networking applications -> [*] netcat
Target packages -> Networking applications -> [*] ntp
Target packages -> Networking applications -> ntp -> [*] ntpd
```

## Netwerk-functionaliteit aan de broedmachine-software toev

```bash
ifconfig eth1 10.42.0.2 netmask 255.255.255.0
route add default gw 10.42.0.1
echo "nameserver 8.8.8.8" > /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf
/etc/init.d/S49ntp restart
```

## Test-gegevens vanaf de broedmachine naar graphite versturen

```sh
echo "temperature 0 `date +%s`" | nc example.com 2003 -c
echo "humidity 0 `date +%s`" | nc example.com 2003 -c
```
